
import * as _ from "lodash";

export default class StringHelper {
    ensureText = (obj?: string, trimText?: boolean) => {
        const defaultText = "";
        if (_.isNull(obj)) return defaultText;
        let s = _.isNull(obj) || trimText ? obj.trim() : obj;
        return s || defaultText;
    };
}
