import * as React from "react";
import AutoDropComplete from "../AutoDropComplete/AutoDropComplete";

export interface HelloProps { compiler: string; framework: string; }

export const Hello = (props: HelloProps) => <>
        <h1>Hello from {props.compiler} and {props.framework}!</h1>
        <AutoDropComplete initialText="" emptyWhenNotMatched={true} data={null} />
    </>;

/*

// class style:
//
// 'HelloProps' describes the shape of props.
// State is never set so we use the '{}' type.
export class Hello extends React.Component<HelloProps, {}> {
    render() {
        return <h1>Hello from {this.props.compiler} and {this.props.framework}!</h1>;
    }
}

*/
